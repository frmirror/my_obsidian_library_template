
# 第二期 Zotero

## Zotero安装和插件配置

Zotero：https://www.zotero.org/

- 插件
	- Zotfile：http://zotfile.com/
	
	- Zotero-better-bibtex：https://github.com/retorquere/zotero-better-bibtex
	
	- 搭配obsidian的zotlit：https://github.com/windingwind/zotero-better-notes

全部安装完之后配置一下zotero和对应的插件


## 配置zolit


### 模板配置

- 文件名
`<%= it.title %>.md`

- Note Content - zt-note.eta


```
# <%= it.title %>

## Zotero链接

[<%= it.title %>](<%= it.backlink %>) <%= it.fileLink %>

## 注释

<%~ include("annots", it.annotations) %>

<% it.content %>


```

- Note Properties - zt-field.eta

```

title: "<%= it.title %>"
citekey: "<%= it.citekey %>"
tags: 
- "Zotero/<%= it.itemType %>"
```

- Single Annations - zt-annot.eta

```
[!abstract] [Page <%= it.pageLabel %>](<%= it.backlink %>) 
<mark style="
<%- if (it.color) { _%> color: <%= it.color %>; <%_ } -%>
<%- if (it.bgColor) { _%> background-color: <%= it.bgColor %>; <%_ } -%>
"><%= it.imgEmbed %><%= it.text %></mark>
<% if (it.comment) { %> 
---
<%= it.comment %>
<% } %>
```


- colored highlight - zt-colored.eta

```
<mark style="
<%- if (it.color) { _%> color: <%= it.color %>; <%_ } -%>
<%- if (it.bgColor) { _%> background-color: <%= it.bgColor %>; <%_ } -%>
"><%= it.content %></mark>
```

